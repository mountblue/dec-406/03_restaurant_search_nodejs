'use strict';

/**
 * name:        restaurant_search.js
 * description: A Restaurant search JS script
 * input:       .csv file containing the restaurant data
 * output:      '\n' delimited String
 * usage:       node restaurant_search --filename | -f <filename>
 *                                     --datetime | -d <datetime>
 * example:     node restaurant_search --filename|-f 'file.csv' --datetime|-d 'dd-mm-yyyy ttam|pm'
 */
const fs = require('fs'),
      cli = require('command-line-args'),
      clu = require('command-line-usage');

/**
* define command-line-args options & create object
* documentation @ https://www.npmjs.com/package/command-line-args
*/
const optionsDefs = [
    {"name": "filename", "alias": "f", "type": String},
    {"name": "datetime", "alias": "d", "type": String}
];

/**
* define command-line-usage guides and check usage
* documentation @ https://www.npmjs.com/package/command-line-usage
*/
const sections = [
    {
        "header": "A Restaurant search JS script",
        "content": "A small program to search open restaurants from a .csv input file"
    },
    {
        "header": "Options",
        "optionList": [
            {
                "name": "filename",
                "alias": "f",
                "typeLabel": "{underline file}",
                "description": "The input to process. The file should be in the same folder as the JS script"
            },
            {
                "name": "datetime",
                "alias": "d",
                "typeLabel": "{underline string} use - / ' ' divider",
                "description": "Check open restaurants on the given date"
            }
        ]
    }
];

let options = {};

// validate user input, handle incorrect values/usage
try {
    options = cli(optionsDefs);
} catch (err) {
    if (err !== null) {
        console.log('Invalid Usage');
        console.error(err.message);
    }
} finally {
    // call check usage before begining to parse data
    checkCliInput(options);
}

function checkCliInput(options) {
    if ((typeof options !== 'Object') && !(('filename' in options) && ('datetime' in options))) {
        console.error('required options missing');
        console.log(clu(sections));
        process.exit();
    }
}

// input path
const PATH = __dirname + '/' + options.filename;

// Parse input data from .csv file
process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

// create read stream to process input data
const readStream = fs.createReadStream(PATH, 'utf8');

readStream.on('data', inputLine => {
    inputString += inputLine;
});

readStream.on('end', _ => {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.trim());

    main();
});

function readLine() {
    return inputString[currentLine++];
}

/**
 * Primary function to search open restaurants given the date and time.
 * @params: restaurants Object, datetime String
 * @return: Array restaurant names
 */
function restaurantSearch(restaurants, datetime) {
    let dayData = parseInputDate(datetime);
    let output = [];

    // console.log(dayData);

    for (let i = 0, n = restaurants.length; i < n; i++) {
        let day = dayData.day,
            time = dayData.time;

        // console.log(day);

        if (day in restaurants[i].open) {
            // console.log(restaurants[i].open[day].start);
            if (time >= restaurants[i].open[day].start && time <= restaurants[i].open[day].stop) {
                output.push(restaurants[i].name);
            }
        }
    }

    return output;
}

// convert time to 24-hour format
function militaryHour(hour, type) {
    let militaryHour = null;
    let minute = 0;

    if (/:/.test(hour)) {
        let time = hour.match(/(\d\d?):(\d\d?)/);
        hour = time[1];
        minute = time[2];
    }
    if (type === 'pm') {
        militaryHour = +hour + 12 + ':' + minute;
        if (militaryHour == 24)
            militaryHour = 12 + ':' + minute;
    } else {
        militaryHour = +hour + ':' + minute;
        if (militaryHour == 12)
            militaryHour = 0 + ':' + minute;
    }
    return militaryHour;
}

// Parse the date from command-line-args datetime input
function parseInputDate(datetime) {
    const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

    let time = datetime.match(/(\d\d?)[\/-\s](\d\d?)[\/-\s](\d\d\d\d) (\d\d?:?\d?\d?)[\s]?(pm|am)/i);

    if (time == null) {
        console.error('Invalid input/Incorrect values. Format dd-mm-yyyy tt am|pm. Acceptable seperators [- / or space " "]')
        process.exit();
    }

    let milHour = militaryHour(time[4], time[5]);
    let dateObj = new Date(time[2] + " " + time[1] + " " + time[3] + " " + milHour);

    if (dateObj == 'Invalid Date') {
        console.error('Invalid date. Format dd-mm-yyyy tt am|pm. Acceptable seperators [- / or space " "]')
        process.exit();
    }

    let dayData ={};
    dayData.day = days[dateObj.getDay()];
    dayData.time = dateObj.getHours() + ':' + dateObj.getMinutes();

    return dayData;
}

/**
 * function to create restaurant data object to simulate MongoDB style document
 * Properties: name String, open Object
 * @prop open Properties: dayName Object
 * @return restaurant data Object
 */
function restaurantDoc(restaurantData) {
    const days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    let restDoc = {
        "name": null,
        "open": {}
    };
    let workDays = {};
    let workHours = {};

    restDoc.name = restaurantData[0];

    let time = restaurantData[1].split('/').map(str => str.trim());

    for (let i = 0, n = time.length; i < n; i++) {
        let hours = time[i].match(/(\d\d?:?\d?\d?) ([a-z]{2}) - (\d\d?:?\d?\d?) ([a-z]{2})/i);
        workHours.start = militaryHour(hours[1], hours[2]),
        workHours.stop = militaryHour(hours[3], hours[4]);
        if (/-/.test(time[0])) {
            let range = time[0].match(/([a-z]{3})-([a-z]{3})/i);
            let startIndex = days.indexOf(range[1]),
                stopIndex = days.indexOf(range[2]);
            for (let i = startIndex; i <= stopIndex; i++) {
                workDays[days[i]] = workHours;
            }
        }

        let day = time[i].match(/\s([a-z]{3})\s/i);
        if (day) {
            workDays[day[1]] = workHours;
        }
    }
    restDoc.open = workDays;

    return restDoc;
}

// Main function
function main() {
    const datetime = options.datetime,
          n = inputString.length;

    let restaurants = [];
    // console.log(n);

    for (let i = 0  ; i < n; i++) {
        let restaurantData = readLine().replace(',', '#').split('#');
        restaurants[i] = restaurantDoc(restaurantData);
    }

    let result = restaurantSearch(restaurants, datetime);

    process.stdout.write(result.join('\n') + '\n');

    process.exit();
}
